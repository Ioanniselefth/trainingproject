package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class CalculatorTest {
    //test calculator class
    @Test
    public void test_first() throws Exception {
        Calculator mycal = new Calculator();
        assertEquals(9, mycal.add());
    }

    @Test
    public void test_second() throws Exception {
        Calculator mycal = new Calculator();
        assertNotEquals(10, mycal.add());
    }
}
