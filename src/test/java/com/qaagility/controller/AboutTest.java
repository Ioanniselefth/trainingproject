package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class AboutTest {
    //test about class
    @Test
    public void test_first() throws Exception {
        About myabout = new About();
        assertEquals("This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!", myabout.desc());
    }

}
