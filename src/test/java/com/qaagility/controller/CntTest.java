package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class CntTest {
    //test Cnt class
    @Test
    public void test_first() throws Exception {
        CntClass mycnt = new CntClass();
        assertEquals(2, mycnt.dMethodD(4, 2));
    }

    @Test
    public void test_second() throws Exception {
        CntClass mycnt = new CntClass();
        assertEquals(3, mycnt.dMethodD(6, 2));
    }

    @Test
    public void test_third() throws Exception {
        CntClass mycnt = new CntClass();
        assertEquals(Integer.MAX_VALUE, mycnt.dMethodD(4, 0));
    }
}
