package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class CalcmulTest {
    
    @Test
	public void test_first() throws Exception {
        //test Calcmul.java
        Calcmul mycalmul = new Calcmul();
        assertEquals(18, mycalmul.mul());
	}

    @Test
	public void test_second() throws Exception {
        //test Calcmul.java
        Calcmul mycalmul = new Calcmul();
        assertNotEquals(20, mycalmul.mul());
    }
}
